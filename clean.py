"""Check folder to find video files which are corrupted.

Corrupt files can optionally be removed automatically too.
"""

import argparse
import logging
import mimetypes
import os
import subprocess
import sys

FORMAT = '%(asctime)-15s %(levelname)-7s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)
log = logging.getLogger()

def parse_args():
    """Parse args from cmdline."""
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('path', help='Root path to search under')
    parser.add_argument(
        '--rm',
        help='Remove any corrupt video files',
        action='store_true')
    return parser.parse_args()

def check_file(filepath):
    """Return True unless there are issues with the video file.

    Use ffmpeg transcode to identify any issues.

    :param filepath: Path to the file to inspect.
    """
    try:
        subprocess.run(
            ['ffmpeg', '-v', 'error', '-i', filepath, 
             '-map', '0:1', '-f', 'null', '-'],
            check=True)
    except subprocess.CalledProcessError:
        return False
    else:
        return True

def is_video(filepath):
    """Use mimetype to return boolean indicating if file is a video.

    :param filepath: Path to the file to inspect.
    """
    try:
        return 'video' in mimetypes.guess_type(filepath)[0]
    except TypeError:
        log.warning("Unable to determine filetype for %s", filepath)
        return False


def main():
    args = parse_args()
    if args.verbose:
        log.setLevel(logging.DEBUG)
    mimetypes.init()
    log.debug("Debug logging enabled.")
    log.info("Processing files under %s", args.path)

    for root, dirs, files in os.walk(args.path, topdown=True):
        for name in files:
            filepath = os.path.join(root, name)

            if not is_video(filepath):
                log.debug("%s is not a video file, ignoring", filepath)
                continue

            log.debug("Checking %s", filepath)
            if check_file(filepath):
                log.debug('Valid: %s', filepath)
                continue
            log.info('Corrupt: %s', filepath)
            if args.rm:
                log.info("Removing %s")
                os.remove(filepath)


if __name__ == '__main__':
    main()